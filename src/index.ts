import VocabularyTrainer from "./app";

import {
  setErrorSymbol,
  setFinalMessage,
  setTotalQuestions,
  setCurrentQuestion,
  setWordButtons,
  setGameStatus,
  addSymbol,
  setNewButton,
  restoreWord,
} from "./dom";

/**
 * Init Vocabulary Trainer app
 */
const Trainer = new VocabularyTrainer({});

const handleSelectSymbol = (letter: string, index: number) => {
  Trainer.selectLetter(letter, index);
};

const handleTypeSymbol = (key: string) => {
  const index = Trainer.shuffledWord.indexOf(key);
  handleSelectSymbol(key, index);
};

const handleKeydown = ({ key }: KeyboardEvent) => {
  if (key === 'ArrowLeft') {
    Trainer.prevGame();
  } else if (key === "ArrowRight") {
    Trainer.nextGame();
  }

  if (key.length === 1) {
    handleTypeSymbol(key);
  }
}

Trainer.on("error", (event) => {
  const { detail } = event;
  setErrorSymbol(detail.letter, detail.letterIndex);
});

Trainer.on("end", ({ detail }) => {
  setGameStatus(detail);
  setNewButton(Trainer.newGame);

  if (detail.reason === "fail") {
    setFinalMessage(Trainer.currentWord, 'fail');
    setWordButtons("", handleSelectSymbol);
  }

  window.removeEventListener("keydown", handleKeydown);
});

Trainer.on("successLetter", ({ detail }) => {
  addSymbol(detail.letter, detail.letterIndex);
});

Trainer.on("step", (event) => {
  setTimeout(() => {
    setWordButtons(event.detail, handleSelectSymbol);
    setCurrentQuestion(Trainer.currentStep + 1);
  }, 500);
});

Trainer.on("start", ({ detail }) => {
  setTotalQuestions(detail.totalSteps);
  setCurrentQuestion(detail.currentStep + 1);
  setWordButtons(detail.shuffledWord, handleSelectSymbol);
  restoreWord(detail.shuffledWord, detail.currentWord, handleSelectSymbol);

  window.addEventListener("keydown", handleKeydown);
});

Trainer.on("history", ({ detail }) => {
  setTotalQuestions(detail.totalSteps);
  setCurrentQuestion(detail.currentStep + 1);

  if (detail.gameState !== 'process') {
    setFinalMessage(detail.currentWord, detail.gameState)
  } else {
    setWordButtons(detail.shuffledWord, handleSelectSymbol);
    restoreWord(detail.shuffledWord, detail.currentWord, handleSelectSymbol);
  }

  setGameStatus(detail?.statistics);
});

window.onload = () => {
  Trainer.startGame();
};
