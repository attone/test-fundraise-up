import { VocabularyTrainerInterface } from "../types";

const CACHE_NAME = "cache";
const HISTORY_CACHE_NAME = "history";

export const getLocalStorage = <T>(name: string): T => {
  const cachedData = localStorage.getItem(name);
  if (!cachedData) return null;
  return JSON.parse(cachedData) as T;
};

export const getCache = (): VocabularyTrainerInterface | null => {
  return getLocalStorage<VocabularyTrainerInterface>(CACHE_NAME) || null;
};

/**
 * Save to cache only game result after ending
 */
export const saveCache = (data: VocabularyTrainerInterface) => {
  localStorage.setItem(CACHE_NAME, JSON.stringify(data));
};

export const addHistory = (data: VocabularyTrainerInterface) => {
  const cache =
    getLocalStorage<VocabularyTrainerInterface[]>(HISTORY_CACHE_NAME) || [];
  cache.unshift(data);
  localStorage.setItem(HISTORY_CACHE_NAME, JSON.stringify(cache));
};

export const getHistory = (): VocabularyTrainerInterface[] => {
  const cachedData = localStorage.getItem(HISTORY_CACHE_NAME);
  if (!cachedData) return [];
  return JSON.parse(cachedData) as VocabularyTrainerInterface[];
};

/**
 * Get old game results
 */
export const getCacheItem = (
  index?: number,
): VocabularyTrainerInterface | null => {
  const cache =
    getLocalStorage<VocabularyTrainerInterface[]>(HISTORY_CACHE_NAME) || [];
  return cache?.[index] || cache?.pop() || null;
};

/**
 * Clear cache
 */
export const clearCache = (name: string) => {
  localStorage.removeItem(name);
};
