interface WordsInterface {
  [word: string]: number;
}

export const getWrongestWord = (words: string[]): string => {
  const totalErrors: WordsInterface = {};
  let maxCount = 0;
  let maxElement = "";

  words.forEach((item) => {
    totalErrors[item] = (totalErrors[item] || 0) + 1;

    if (totalErrors[item] > maxCount) {
      maxCount = totalErrors[item];
      maxElement = item;
    }
  });

  return maxElement;
};

export const getCleanWordsCount = (
  words: string[],
  totalWords: number,
): number => {
  const uniqueErrors = [...new Set(words)];
  return totalWords - uniqueErrors.length;
};
