# Fundraise up test task

> UI logic in task.md file

![Vocabulary Trainer App screenshot](./public/screenshot.png)

### Start dev version

```sh
yarn install
yarn start
```

### Build

```sh
yarn install
yarn build
```

***

## Vocabulary Trainer App

To initialize game:

```typescript
import VocabularyTrainer from "./app";

const Trainer = new VocabularyTrainer({});
```

***

### Vocabulary Trainer listeners:

#### error:

Calls when select wrong symbol

```typescript
Trainer.on("error", (event) => {
  event.detail: LetterInterface;
})
```

**Error event type:**

> ```typescript
> interface LetterInterface {
>   word: string;         // Shuffled word
>   step: number;         // Current step
>   letterIndex: number;  // Letter index in shuffled word
>   letter: string;       // letter symbol
> }
> ```

#### successLetter:

Calls when select correct symbol

> ```typescript
> Trainer.on("successLetter", (event) => {
>   event.detail: LetterInterface;
> })
> ```

**Success Letter event type:**

> ```typescript
> interface LetterInterface {
>   word: string;         // Shuffled word
>   step: number;         // Current step
>   letterIndex: number;  // Letter index in shuffled word
>   letter: string;       // letter symbol
> }
> ```

#### step:

Calls on new step

> ```typescript
> Trainer.on("step", (event) => {
>   event.detail: string;   // returns new shuffled word
> })
> ```

#### end:

Calls on game end

> ```typescript
> Trainer.on("end", (event) => {
>   event.detail: StatisticsInterface // return statistics
> })
> ```

**Statistics interface:**

> ```typescript
> interface StatisticsInterface {
>  reason: GameState;       // Reason to end the game: "success" | "fail"
>  wrongestWord: string;    // Wrongest word
>  totalErrors: number;     // Total errors
>  cleanWordsCount: number; // Words without errors
> ```

#### start:

Calls on game start

> ```typescript
> Trainer.on("start", (event) => {
>   event.detail: VocabularyTrainerInterface // returns current game state
> })
> ```

**Vocabulary Trainer interface:**

> ```typescript
> VocabularyTrainerInterface {
>   /**
>    * Current question index
>    */
>   readonly currentStep: number;
>   /**
>    * Current filled word
>    */
>   readonly currentWord: string;
>   /**
>    * Shuffled word
>    */
>   readonly shuffledWord: string;
>   /**
>    * Type errors
>    */
>   readonly errors: LetterInterface[];
>   /**
>    * The end game status
>    */
>   readonly gameState: GameState;
>   /**
>    * List of words
>    */
>   readonly wordsList: string[];
>   /**
>    * Maximum available errors for game
>    */
>   readonly maxErrors: number;
>   /**
>    * Total questions
>    */
>   readonly totalSteps: number;
>   /**
>    * Statistics
>    */
>   readonly statistics?: StatisticsInterface;
> }
> ```

#### history:

Calls on switch games history

> ```typescript
> Trainer.on("history", (event) => {
>   event.detail: VocabularyTrainerInterface // returns history game state
> })
> ```

> **Note:** It caching results in localStorage, but if you select history after start it returns current game state, not history

***

### Actions:

#### newGame

Init new game and calls **start** event

Doesn't save current game in history

> ```typescript
> Trainer.newGame()
> ```

#### startGame

Restore saved cache (current game state) from localStorage or start new game

> ```typescript
> Trainer.startGame()
> ```

#### selectLetter

Call it on select letter button, it starts validation and calls **error** or **successLetter** event

If it last step or maximum errors is achieve it also calls **end** with reason and statistics

> ```typescript
> Trainer.selectLetter()
> ```

***

### History navigation:

#### prevGame

Find previuos game result in history and calls **history** event

> ```typescript
> Trainer.prevGame()
> ```

#### nextGame

Find next game result in history and calls **history** event

> ```typescript
> Trainer.nextGame()
> ```
