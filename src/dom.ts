import type { StatisticsInterface } from "./app/types";

export const animateClass = (element: HTMLElement, className: string) => {
  element.classList.add(className);

  setTimeout(() => {
    element.classList.remove(className);
  }, 300);
};

export const setErrorSymbol = (letter: string, index: number) => {
  const buttonsWrapper = document.querySelector("#letters");
  const button =
    buttonsWrapper.querySelector<HTMLButtonElement>(
      `button[data-index="${index}"]`,
    ) ||
    buttonsWrapper.querySelector<HTMLButtonElement>(
      `button[data-value="${letter}"]`,
    );

  if (button) {
    animateClass(button, "btn-danger");
  }
};

export const setTotalQuestions = (count: number) => {
  const totaQuestionsWrapper = document.querySelector<HTMLSpanElement>(
    "span#total_questions",
  );
  totaQuestionsWrapper.innerText = `${count}`;
};

export const setCurrentQuestion = (step: number) => {
  const currentWordWrapper = document.querySelector<HTMLDivElement>("#answer");
  const currentQuestionWrapper = document.querySelector("#current_question");

  currentQuestionWrapper.innerHTML = `${step}`;
  currentWordWrapper.innerHTML = null;
};

export const addSymbol = (letter: string, index: number) => {
  const currentWordWrapper = document.querySelector("#answer");
  const buttonsWrapper = document.querySelector("#letters");
  const button =
    buttonsWrapper.querySelector(`button[data-index="${index}"]`) ||
    buttonsWrapper.querySelector(`button[data-value="${letter}"]`);

  const letterWrapper = document.createElement("span");
  letterWrapper.className = "btn btn-success";
  letterWrapper.innerText = letter;
  currentWordWrapper.appendChild(letterWrapper);
  animateClass(letterWrapper, "btn-info");
  button?.remove();
};

export const setFinalMessage = (word: string, type: 'fail' | 'success') => {
  const currentWordWrapper = document.querySelector("#answer");
  const wordButtonsWrapper = document.querySelector("#letters");
  currentWordWrapper.innerHTML = null;
  wordButtonsWrapper.innerHTML = null;

  for (let i = 0; i < word.length; i++) {
    const letterWrapper = document.createElement("span");
    letterWrapper.className = "btn";
    letterWrapper.classList.add(type === 'fail' ? 'btn-danger' : 'btn-success');
    letterWrapper.innerText = word[i];
    currentWordWrapper.appendChild(letterWrapper);
  }
};

export const setWordButtons = (
  word: string,
  callback: (symbol: string, index: number) => void,
) => {
  const buttonsWrapper = document.querySelector("#letters");
  buttonsWrapper.innerHTML = null;

  for (let i = 0; i < word.length; i++) {
    const symbol = word[i];
    const button = document.createElement("button");

    button.className = "btn";
    button.setAttribute("data-index", `${i}`);
    button.setAttribute("data-value", symbol);
    button.innerText = symbol;
    button.onclick = () => {
      callback(symbol, i);
    };

    buttonsWrapper.appendChild(button);
  }
};

export const restoreWord = (
  shuffledWord: string,
  currentWord: string,
  callback: (symbol: string, index: number) => void,
) => {
  let newWord = shuffledWord;

  for (let i = 0; i < currentWord.length; i++) {
    const letter = currentWord[i];
    addSymbol(letter, i);

    const index = shuffledWord.indexOf(letter);
    newWord = newWord.slice(0, index) + newWord.slice(index + 1);
  }

  for (let i = 0; i < newWord.length; i++) {
    setWordButtons(newWord, callback);
  }
};

export const setNewButton = (onclick: () => void) => {
  const wrapper = document.querySelector("#new_game");
  const button = document.createElement("button");
  button.classList.add("btn");
  button.innerText = 'New game';
  button.onclick = (event) => {
    event.preventDefault();

    const statWrapper = document.querySelector("#game_statistics");
    statWrapper.innerHTML = null;
    onclick();
    button.remove();
  };
  wrapper.appendChild(button);
}

export const setGameStatus = (data: StatisticsInterface) => {
  const statWrapper = document.querySelector("#game_statistics");
  statWrapper.innerHTML = null;

  if (!data) return;

  const createParam = (text: string) => {
    const statItem = document.createElement("li");
    statItem.classList.add("list-group-item");
    statItem.innerText = text;
    statWrapper.appendChild(statItem);
  };

  /**
   * Show words without errors
   */
  createParam(data.reason);

  /**
   * Show words without errors
   */
  createParam(`Words without errors: ${data.cleanWordsCount}`);

  /**
   * Set total errors count
   */
  createParam(`Total errors: ${data.totalErrors}`);

  /**
   * Show word with maximum errors
   */
  if (data.wrongestWord) {
    createParam(`Word with maximum errors: ${data.wrongestWord}`);
  }
};
