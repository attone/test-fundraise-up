export type GameState = "process" | "success" | "fail";

export interface LetterInterface {
  word: string;
  step: number;
  letterIndex: number;
  letter: string;
}

export interface StatisticsInterface {
  reason: GameState;
  /**
   * Wrongest word
   */
  wrongestWord: string;
  /**
   * Total errors count
   */
  totalErrors: number;
  /**
   * Words without errors
   */
  cleanWordsCount: number;
}

export interface VocabularyTrainerProps {
  maxErrors?: number;
}

export interface VocabularyTrainerInterface {
  /**
   * Current question index
   */
  readonly currentStep: number;
  /**
   * Current filled word
   */
  readonly currentWord: string;
  /**
   * Shuffled word
   */
  readonly shuffledWord: string;
  /**
   * Type errors
   */
  readonly errors: LetterInterface[];
  /**
   * The end game status
   */
  readonly gameState: GameState;
  /**
   * List of words
   */
  readonly wordsList: string[];
  /**
   * Maximum available errors for game
   */
  readonly maxErrors: number;
  /**
   * Total questions
   */
  readonly totalSteps: number;
  /**
   * Statistics
   */
  readonly statistics?: StatisticsInterface;
}

export interface EventListeners {
  /**
   * Error message
   */
  error: LetterInterface;
  /**
   * Is success letter
   */
  successLetter: LetterInterface;
  /**
   * Returns new word
   */
  step: string;
  /**
   * Reason why game is end
   */
  end: StatisticsInterface;
  /**
   * Returns current game state
   */
  start: VocabularyTrainerInterface;
  /**
   * Returns history game items
   */
  history: VocabularyTrainerInterface;
}
