import { words } from "./constants/words";

import shuffleArray, { shuffleString } from "./utils/shuffle";
import { getWrongestWord, getCleanWordsCount } from "./utils/statistics";

import {
  GameState,
  LetterInterface,
  EventListeners,
  VocabularyTrainerProps,
  VocabularyTrainerInterface,
  StatisticsInterface,
} from "./types";
import { addHistory, getCache, getHistory, saveCache } from "./utils/cache";

class VocabularyTrainer implements VocabularyTrainerProps {
  private eventTarget = new EventTarget();

  /**
   * Maximum available errors for game
   */
  readonly maxErrors: number;
  /**
   * Total questions
   */
  readonly totalSteps: number;
  /**
   * Current question index
   */
  currentStep: number;
  /**
   * Current filled word
   */
  currentWord: string;
  /**
   * Shuffled word
   */
  shuffledWord: string;
  /**
   * Type errors
   */
  errors: LetterInterface[];
  /**
   * The end game status
   */
  gameState: GameState;
  /**
   * List of words
   */
  private wordsList: string[];
  /**
   * History item
   */
  private historyIndex: number;
  /**
   * Statistics
   */
  private statistics?: StatisticsInterface;

  constructor({ maxErrors = 3, totalSteps = 6 }) {
    this.totalSteps = totalSteps;
    this.maxErrors = maxErrors;
    this.historyIndex = -1;

    this.newGame = this.newGame.bind(this);
    this.saveCache = this.saveCache.bind(this);
    this.saveHistory = this.saveHistory.bind(this);
  }

  private getState() {
    return {
      currentStep: this.currentStep,
      errors: this.errors,
      currentWord: this.currentWord,
      wordsList: this.wordsList,
      shuffledWord: this.shuffledWord,
      gameState: this.gameState,
      totalSteps: this.totalSteps,
      maxErrors: this.maxErrors,
      statistics: this.statistics,
    };
  }

  /**
   * Save cache
   */
  private saveCache() {
    saveCache(Object.assign({}, this.getState()));
  }

  /**
   * Add to history
   */
  private saveHistory() {
    addHistory(Object.assign({}, this.getState()));
  }

  /**
   * Event listener for use like
   *
   * on('error', (message) => { ... })
   */
  on<K extends keyof EventListeners>(
    type: K,
    listener: (event: CustomEvent<EventListeners[K]>) => void,
  ) {
    this.eventTarget.addEventListener(type, listener);
  }

  private triggerEvent<K extends keyof EventListeners>(
    type: keyof EventListeners,
    data: EventListeners[K],
  ) {
    const event = new CustomEvent(type, {
      detail: data,
    });
    this.eventTarget.dispatchEvent(event);
  }

  /**
   * Get game statistics
   */
  private getStatistics(data: VocabularyTrainerInterface) {
    if (!data) return null;

    const errorWords = data?.errors.map(({ step }) => data?.wordsList[step]);

    return {
      reason: data.gameState,
      cleanWordsCount: getCleanWordsCount(errorWords, data?.totalSteps),
      totalErrors: data.errors.length,
      wrongestWord: getWrongestWord(errorWords),
    };
  }

  /**
   * History
   */
  public prevGame() {
    const cache = getHistory();
    const prevIndex = this.historyIndex + 1;

    if (prevIndex === cache.length) {
      return;
    }
    if (cache.length < 1 || !cache[prevIndex]) return;
    else {
      this.triggerEvent("history", cache[prevIndex]);
      this.historyIndex += 1;
    }
  }

  public nextGame() {
    const cache = getHistory();
    const nextIndex = this.historyIndex - 1;

    if (nextIndex < 0) {
      this.triggerEvent("history", this.getState());
      this.historyIndex = -1;
    } else {
      this.triggerEvent("history", cache[nextIndex]);
      this.historyIndex -= 1;
    }
  }

  /**
   * New game
   */
  public newGame() {
    this.currentStep = 0;
    this.errors = [];
    this.currentWord = "";
    this.wordsList = shuffleArray(words).slice(0, this.totalSteps);
    this.gameState = "process";
    this.shuffleCurrentWord();
    this.statistics = null;

    this.triggerEvent("start", this.getState());
  }

  public startGame() {
    this.restoreCache();
  }

  /**
   * Restore cache if exists
   */
  private restoreCache() {
    const cachedValues = getCache();

    /**
     * Check if cache is exists
     */
    if (cachedValues?.gameState === "process") {
      this.currentStep = cachedValues.currentStep;
      this.errors = cachedValues.errors;
      this.currentWord = cachedValues.currentWord;
      this.wordsList = cachedValues.wordsList;
      this.shuffledWord = cachedValues.shuffledWord;
      this.gameState = cachedValues.gameState;
      this.triggerEvent("start", this.getState());
    } else {
      this.newGame();
    }
  }

  private shuffleCurrentWord() {
    const currentWord = this.wordsList[this.currentStep];
    this.shuffledWord = shuffleString(currentWord);
  }

  private endGame(reason: GameState) {
    this.gameState = reason;
    this.currentWord = this.wordsList[this.currentStep];
    const statistics = this.getStatistics(this.getState());
    this.statistics = statistics;
    this.saveHistory();

    this.triggerEvent("end", statistics);
  }

  private handleNewStep() {
    this.currentStep += 1;
    this.currentWord = "";
    this.shuffleCurrentWord();
    this.triggerEvent("step", this.shuffledWord);
    this.saveCache();
  }

  private setError(error: LetterInterface) {
    this.errors.push(error);
    this.triggerEvent("error", error);
    this.saveCache();
  }

  private setSuccessLetter(symbol: LetterInterface) {
    this.currentWord = `${this.currentWord}${symbol.letter}`;
    this.triggerEvent("successLetter", symbol);
    this.saveCache();
  }

  private setFailGame() {
    this.currentWord = this.wordsList[this.currentStep];
    this.endGame("fail");
  }

  /**
   * Validate ipput symbol
   */
  private validateSymbol(
    letter: string,
    index: number,
  ): Promise<"step" | "letter"> {
    const count = this.currentWord.length;
    const fullWord = this.wordsList[this.currentStep];
    const word = this.shuffledWord;

    const symbol = {
      step: this.currentStep,
      word: this.shuffledWord,
      letterIndex: index,
      letter,
    };

    if (fullWord[count] === letter) {
      this.setSuccessLetter(symbol);

      const isLastLetter = count === fullWord.length - 1;

      if (isLastLetter && this.currentStep < this.totalSteps - 1) {
        this.handleNewStep();
      } else if (isLastLetter) {
        this.endGame("success");
      }

      return;
    }

    this.setError(symbol);

    if (this.errors.length >= this.maxErrors) {
      this.setFailGame();
    }

  }

  public selectLetter(letter: string, index: number) {
    if (this.gameState !== "process") {
      console.log("Game is end");
      return;
    }

    if (letter.length !== 1) {
      console.log("Wrong symbol");
      return;
    }

    this.validateSymbol(letter, index);
  }
}

export default VocabularyTrainer;
